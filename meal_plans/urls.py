from django.urls import path

from meal_plans.views import (
    MealPlanCreateView,
    MealPlanListView,
    MealPlanDetailView,
    MealPlanEditView,
    MealPlanDeleteView
)


urlpatterns = [
    path("", MealPlanListView.as_view(), name="meal_plans_list"),

    path("<int:pk>/", MealPlanDetailView.as_view(), name="meal_plans_detail"),

    path("<int:pk>/delete/", MealPlanDeleteView.as_view(),
         name="meal_plans_delete"),

    path("create/", MealPlanCreateView.as_view(), name="meal_plans_create"),

    path("<int:pk>/edit/", MealPlanEditView.as_view(), name="meal_plans_edit"),
]
